package commander;

import commander.config.AppConfig;
import commander.handler.CommandHandler;
import commander.model.Command;
import commander.model.User;

import java.util.Map;
import java.util.Scanner;

import static commander.CommandParser.EXIT_COMMAND;

public class Main {

    private AuthManager authManager;
    private CommandParser commandParser;
    private Map<String, CommandHandler> commandHandlers;

    public Main(AuthManager authManager, CommandParser commandParser, Map<String, CommandHandler> commandHandlers) {
        this.authManager = authManager;
        this.commandParser = commandParser;
        this.commandHandlers = commandHandlers;
    }

    public static void main(String[] args) {
        AppConfig appConfig = new AppConfig();

        Main main = new Main(appConfig.authManager(), appConfig.commandParser(), appConfig.commandHandlers());

        main.play();
    }

    public void play() {
        Scanner scanner = new Scanner(System.in);

        System.out.println("User name:");
        String userName = scanner.nextLine();
        System.out.println("Password:");
        String password = scanner.nextLine();

        User user = authManager.auth(userName, password);

        Command command;
        do {
            command = commandParser.parse(scanner.nextLine());

            if (notExit(command)) {
                if (user.getCommands().contains(command.getCommandName())) {
                    CommandHandler commandHandler = commandHandlers.get(command.getCommandName());
                    commandHandler.handle(command);
                } else {
                    throw new IllegalArgumentException("You are not allowed to execute this command");
                }
            }

        } while (notExit(command));
    }

    private boolean notExit(Command command) {
        return !EXIT_COMMAND.equals(command);
    }
}
