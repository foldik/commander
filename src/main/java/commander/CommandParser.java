package commander;

import commander.model.Command;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class CommandParser {

    public static final Command EXIT_COMMAND = new Command("x", Collections.emptyList());

    public Command parse(String input) {
        input = input.trim();
        if ("x".equals(input)) {
            return EXIT_COMMAND;
        } else {
            int openingBracketIndex = input.contains("[") ? input.indexOf("[") : input.length();
            String commandName = input.substring(0, openingBracketIndex).trim();
            String argsPart = input.substring(openingBracketIndex);
            List<String> commandArguments = getCommandArguments(argsPart);
            return new Command(commandName, commandArguments);
        }
    }

    private List<String> getCommandArguments(String argsPart) {
        if (argsPart == null || argsPart.length() == 0) {
            return Collections.emptyList();
        } else {
            String[] arguments = argsPart.replace("[", "").replace("]", "").split(",");
            List<String> processedArguments = new ArrayList<>();
            for (String arg : arguments) {
                processedArguments.add(arg.trim());
            }
            return processedArguments;
        }
    }
}
