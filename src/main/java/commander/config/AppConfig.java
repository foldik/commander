package commander.config;

import commander.AuthManager;
import commander.CommandParser;
import commander.handler.*;
import commander.model.User;

import java.util.*;

import static commander.util.Hash.sha256;

public class AppConfig {

    public AuthManager authManager() {
        return new AuthManager(Arrays.asList(
                new User("admin", sha256("admin"), allowedCommands("time", "rand", "sum", "min", "max", "zip")),
                new User("user1", sha256("user1"), allowedCommands("time", "rand", "min", "max"))
        ));
    }

    public CommandParser commandParser() {
        return new CommandParser();
    }

    public Map<String, CommandHandler> commandHandlers() {
        Map<String, CommandHandler> commandHandlers = new HashMap<>();
        commandHandlers.put("time", new TimeCommandHandler());
        commandHandlers.put("rand", new RandomGeneratorCommandHandler());
        commandHandlers.put("sum", new SumCommandHandler());
        commandHandlers.put("min", new MinCommandHandler());
        commandHandlers.put("max", new MaxCommandHandler());
        commandHandlers.put("zip", new ZipCommandHandler());
        return commandHandlers;
    }


    private Set<String> allowedCommands(String... commands) {
        return new HashSet<>(Arrays.asList(commands));
    }
}
