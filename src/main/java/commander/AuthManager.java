package commander;

import java.util.List;

import commander.model.AuthenticationException;
import commander.model.User;
import commander.util.Hash;

public class AuthManager {

	private List<User> users;

	public AuthManager(List<User> users) {
		this.users = users;
	}

	public User auth(String name, String password) {
		String hash = Hash.sha256(password);
		for (User user : users) {
			if (user.getUserName().equals(name) 
					&& user.getHash().equals(hash)) {
				System.out.println("Authentication success");
				return user;
			}
		}
		throw new AuthenticationException("Authentication failed");
	}
}
