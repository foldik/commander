package commander.handler;

import commander.model.Command;

public class SumCommandHandler implements CommandHandler {

    @Override
    public void handle(Command command) {
        int sum = 0;
        for (String arg : command.getArgs()) {
            sum += Integer.parseInt(arg);
        }
        System.out.println(sum);
    }

}
