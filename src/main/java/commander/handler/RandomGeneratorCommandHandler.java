package commander.handler;

import commander.model.Command;

import java.util.concurrent.ThreadLocalRandom;

public class RandomGeneratorCommandHandler implements CommandHandler {

    @Override
    public void handle(Command command) {
        System.out.println(ThreadLocalRandom.current().nextDouble());
    }

}
