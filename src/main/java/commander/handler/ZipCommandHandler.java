package commander.handler;

import commander.model.Command;

import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

public class ZipCommandHandler implements CommandHandler {

    @Override
    public void handle(Command command) {
        String zipFileName = getLastArgument(command);
        List<String> filesToCompress = getArgumentsBeforeLast(command);

        try (ZipOutputStream zos = new ZipOutputStream(new FileOutputStream(zipFileName))) {
            for (String file : filesToCompress) {
                ZipEntry zipEntry = new ZipEntry(file);
                zipEntry.setMethod(ZipEntry.DEFLATED);
                zos.putNextEntry(zipEntry);
                zos.write(Files.readAllBytes(Paths.get(file)));
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

        System.out.println("Created " + zipFileName + " compressed file");
        System.out.println("Content: " + filesToCompress);
    }

    private List<String> getArgumentsBeforeLast(Command command) {
        return command.getArgs().subList(0, command.getArgs().size() - 1);
    }

    private String getLastArgument(Command command) {
        return command.getArgs().get(command.getArgs().size() - 1);
    }

}
