package commander.handler;

import commander.model.Command;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class TimeCommandHandler implements CommandHandler {

    private static final DateTimeFormatter DATE_TIME_FORMATTER = DateTimeFormatter.ofPattern("yyyy-MM-dd hh:mm:ss");

    @Override
    public void handle(Command command) {
        System.out.println(DATE_TIME_FORMATTER.format(LocalDateTime.now()));
    }

}
