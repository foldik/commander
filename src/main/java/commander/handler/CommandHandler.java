package commander.handler;

import commander.model.Command;

public interface CommandHandler {

	void handle(Command command);
}
