package commander.handler;

import commander.model.Command;

public class MaxCommandHandler implements CommandHandler {

	@Override
	public void handle(Command command) {
		int max = Integer.parseInt(command.getArgs().get(0));
		for (int i = 1; i < command.getArgs().size(); i++) {
			int number = Integer.parseInt(command.getArgs().get(i));
			if (number > max) {
				max = number;
			}
		}
		System.out.println(max);
	}

}
