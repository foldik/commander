package commander.model;

import java.util.Set;

public class User {

	private String userName;
	private String hash;
	private Set<String> commands;

	public User(String userName, String hash, Set<String> commands) {
		this.userName = userName;
		this.hash = hash;
		this.commands = commands;
	}

	public String getUserName() {
		return userName;
	}

	public String getHash() {
		return hash;
	}

	public Set<String> getCommands() {
		return commands;
	}

	@Override
	public String toString() {
		return "User [userName=" + userName + ", commands=" + commands + "]";
	}

}
