package commander.handler;

import commander.model.Command;

public class MinCommandHandler implements CommandHandler {

    @Override
    public void handle(Command command) {
        int min = Integer.parseInt(command.getArgs().get(0));
        for (int i = 1; i < command.getArgs().size(); i++) {
            int number = Integer.parseInt(command.getArgs().get(i));
            if (number < min) {
                min = number;
            }
        }
        System.out.println(min);
    }

}
