package commander.model;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;

public class Command {

    private String commandName;
    private List<String> args;

    public Command(String commandName, List<String> args) {
        this.commandName = commandName;
        this.args = Collections.unmodifiableList(new ArrayList<>(args));
    }

    public String getCommandName() {
        return commandName;
    }

    public List<String> getArgs() {
        return args;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Command command = (Command) o;
        return Objects.equals(commandName, command.commandName) &&
                Objects.equals(args, command.args);
    }

    @Override
    public int hashCode() {

        return Objects.hash(commandName, args);
    }

    @Override
    public String toString() {
        return "Command [commandName=" + commandName + ", args=" + args + "]";
    }

}
